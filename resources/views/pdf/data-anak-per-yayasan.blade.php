<html lang="en">

<head>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
    <main role="main" class="container">
        <div class="col-12">
            <h1 class="text-center">Informasi Data Anak</h1>
            <h3 class="text-center">
                {{ $judul->yayasan->nama_yayasan }} <br> <br>
            </h3>
            <h5 class="">
                {{ $judul->yayasan->alamat }}
            </h5>
        </div>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">Nama </th>
                    <th scope="col">Tempat/Tgl Lahir </th>
                    <th scope="col">Kelamin</th>
                    <th scope="col">NIK</th>
                    <th scope="col">Alamat</th>
                </tr>
            </thead>
            <tbody>
                @foreach($anak as $data)
                <tr>
                    <td scope="row">{{ $data->nama_anak}}</th>
                    <td>{{ $data->tempat_lahir }}/{{ $data->tanggal_lahir }}</td>
                    <td>{{ $data->jenis_kelamin }}</td>
                    <td>{{ $data->nik }}</td>
                    <td>{{ $data->alamat }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>

    </main>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
</body>

</html>