<html lang="en">

<head>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
    <main role="main" class="container">
        <div class="col-12">
            <h1 class="text-center">Informasi Vaksinasi Anak</h1>
            <h3 class="text-center">
                {{ $judul->dataAnak->nama_anak }} <br> <br>
            </h3>
            <h5 class="">
                Jenis Kelamin: {{ $judul->dataAnak->jenis_kelamin }}
            </h5>
            <h5 class="">
                Umur: {{ $judul->dataAnak->umur }}
            </h5>
            <h5 class="">
                Alamat: {{ $judul->dataAnak->alamat }}
            </h5>
            <h5 class="">
                Dari: {{ $judul->dataYayasan->nama_yayasan }}
            </h5>
        </div>

        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">Pemberi Vaksin</th>
                    <th scope="col">Tempat Vaksin</th>
                    <th scope="col">Vaksin Wajib</th>
                    <th scope="col">Tanggal</th>
                    <th scope="col">Vaksin Tambahan</th>
                    <th scope="col">Tanggal</th>
                    <th scope="col">Catatan</th>
                </tr>
            </thead>
            <tbody>
                @foreach($vaksinasi as $data)
                <tr>
                    <td>{{ $data->pemberi_vaksinasi }}</td>
                    <td>{{ $data->tempat_vaksinasi }}</td>
                    <td>{{ $data->vaksinasi_wajib }}</td>
                    <td>{{ $data->tgl_vaksinasi_wajib }}</td>
                    <td>{{ $data->vaksinasi_tambahan }}</td>
                    <td>{{ $data->tgl_vaksinasi_tambahan }}</td>
                    <td>{{ $data->catatan }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>

    </main>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
</body>

</html>