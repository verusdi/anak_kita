<html lang="en">

<head>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
    <main role="main" class="container">
        <div class="col-12">
            <h1 class="text-center">Informasi Kesehatan Anak</h1>
            <h3 class="text-center">
                {{ $judul->yayasan->nama_yayasan }} <br> <br>
            </h3>
            <h5 class="">
                {{ $judul->yayasan->alamat }}
            </h5>
        </div>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">Nama Anak</th>
                    <th scope="col">Penyakit</th>
                    <th scope="col">Gejala</th>
                    <th scope="col">Catatan Dokter</th>
                    <th scope="col">Obat/Resep</th>
                </tr>
            </thead>
            <tbody>
                @foreach($kesehatan as $data)
                <tr>
                    <td scope="row">{{ $data->dataAnak->nama_anak }}</th>
                    <td>{{ $data->nama_penyakit }}</td>
                    <td>{{ $data->gejala_penyakit }}</td>
                    <td>{{ $data->catatan_dokter }}</td>
                    <td>{{ $data->obat_resep }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </main>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
</body>

</html>