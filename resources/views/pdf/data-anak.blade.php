<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<head>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
    <main role="main" class="container">
        <div class="col-8">
            <h1 class="text-center">Data Anak</h1>
        </div>
        <div class="col-12 border border-dark">
            <h3>Biodata</h3>
            
            <p><b>Nama</b>:
                @foreach($anak as $data)
                {{ $data->nama_anak }}
                @endforeach
            </p>
            <p><b>NIK</b>:
                @foreach($anak as $data)
                {{ $data->nik }}
                @endforeach
            </p>
            <p><b>ID Anak</b>:
                @foreach($anak as $data)
                {{ $data->kode_anak }}
                @endforeach
            </p>
            <p><b>Jenis Kelamin</b>:
                @foreach($anak as $data)
                {{ $data->jenis_kelamin }}
                @endforeach
            </p>
            <p><b>Tempat, Tanggal Lahir</b>:
                @foreach($anak as $data)
                {{ $data->kota_lahir }}, {{ $data->tanggal_lahir }}
                @endforeach
            </p>
            <p><b>Alamat</b>:
                @foreach($anak as $data)
                {{ $data->alamat }}, {{$data->tempat_lahir}}
                @endforeach
            </p>
            <h3>Data Fisik</h3>
            <p><b>Disabilitas</b>:
                @foreach($anak as $data)
                {{ $data->disabilitas }}
                @endforeach
            </p>
            <p><b>Alergi</b>:
                @foreach($anak as $data)
                {{ $data->alergi }}
                @endforeach
            </p>
            <p><b>Golongan Darah</b>:
                @foreach($anak as $data)
                {{ $data->gol_darah }}{{ $data->rhesus }}
                @endforeach
            </p>
            <p><b>Tinggi Badan</b>:
                @foreach($anak as $data)
                {{ $data->tinggi_badan }}( Terakhir diperiksa: {{ $data->tgl_tinggi_badan }} )
                @endforeach
            </p>
            <p><b>Suhu Badan</b>:
                @foreach($anak as $data)
                {{ $data->suhu_badan }}( Terakhir diperiksa: {{ $data->tgl_suhu_badan }} )
                @endforeach
            </p>
            <p><b>Indeks Massa Tubuh</b>:
                @foreach($anak as $data)
                {{ $data->massa_tubuh }}( Terakhir diperiksa: {{ $data->tgl_massa_tubuh }} )
                @endforeach
            </p>
            <p><b>Tekanan Darah</b>:
                @foreach($anak as $data)
                {{ $data->tekanan_darah }}( Terakhir diperiksa: {{ $data->tgl_tekanan_darah }} )
                @endforeach
            </p>
            <p><b>Berat Badan</b>:
                @foreach($anak as $data)
                {{ $data->berat_badan }}( Terakhir diperiksa: {{ $data->tgl_berat_badan }} )
                @endforeach
            </p>
            <p><b>Catatan</b>:
                @foreach($anak as $data)
                {{ $data->catatan }}
                @endforeach
            </p>
        </div>
        <hr />

    </main>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
</body>

</html>