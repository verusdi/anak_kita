<html lang="en">

<head>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
    <main role="main" class="container">
        <div class="col-12">
            <h1 class="text-center">Informasi Vaksinasi Anak</h1>
            <h3 class="text-center">
                {{ $judul->yayasan->nama_yayasan }} <br> <br>
            </h3>
            <h5 class="">
                {{ $judul->yayasan->alamat }}
            </h5>
        </div>
        <table class="table table-bordered" style="margin-left:2%;">
            <thead>
                <tr>
                    <th scope="col">Nama Anak</th>
                    <th scope="col">Pemberi Vaksin</th>
                    <th scope="col">Tempat Vaksin</th>
                    <th scope="col">Vaksin Wajib</th>
                    <th scope="col">Tanggal</th>
                    <th scope="col">Vaksin Tambahan</th>
                    <th scope="col">Tanggal</th>
                    <th scope="col">Catatan</th>
                </tr>
            </thead>
            <tbody>
                @foreach($vaksinasi as $data)
                <tr>
                    <td scope="row">{{ $data->dataAnak->nama_anak }}</th>
                    <td>{{ $data->pemberi_vaksinasi }}</td>
                    <td>{{ $data->tempat_vaksinasi }}</td>
                    <td>{{ $data->vaksinasi_wajib }}</td>
                    <td>{{ $data->tgl_vaksinasi_wajib }}</td>
                    <td>{{ $data->vaksinasi_tambahan }}</td>
                    <td>{{ $data->tgl_vaksinasi_tambahan }}</td>
                    <td>{{ $data->catatan }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </main>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
</body>

</html>