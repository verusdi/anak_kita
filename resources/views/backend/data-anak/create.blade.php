@extends('backend.layouts.app')

@section('content')
<style>
    .form-control::-webkit-input-placeholder {
        font-size: 14px;
        color: darkgrey;
    }

    .form-control::-moz-input-placeholder {
        color: darkgrey;
    }

    .form-control:-ms-input-placeholder {
        color: darkgrey;
    }

    textarea {
        width: 100%;
        height: 80px;
    }
</style>
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Data Anak</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Tambah</a></li>
                <li class="breadcrumb-item active">Data Anak</li>
            </ol>
        </div>
    </div>
    <div class="container-fluid">
        <div class="card">
            <div class="card-body p-b-0">
                <h4 class="card-title">Data Anak</h4>
                @if (session('error'))
                <div class="alert alert-danger">{{ session('error') }}</div>
                @endif
                <!-- Nav tabs -->
                <ul class="nav nav-tabs customtab" role="tablist">
                    <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#data-anak" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Biodata Anak</span></a> </li>
                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#data-fisik" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Data Fisik</span></a> </li>
                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#data-document" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Data Document<span style="color:red;"> *</span></span></a> </li>
                    <!-- <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#kesehatan" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Kesehatan<span style="color:red;"> *</span></span></a> </li> -->
                </ul>
                <!-- Tab panes -->
                <form action="{{ route('data-anak.store') }}" enctype="multipart/form-data" method="post">
                    <div class="tab-content">
                        @csrf
                        <div class="tab-pane active" id="data-anak" role="tabpanel">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="basic-elements">
                                                <div class="row">
                                                    <!--- Rows Kiri -->
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label>ID Anak<span style="color:red;"> *</span></label>
                                                            <input type="number" min="0" name="kode_anak" class="form-control" value="{{ old('kode_anak') }}" placeholder="ID Anak" required>
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('kode_anak'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('kode_anak')}}
                                                                @endif
                                                            </p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>NIK<span style="color:red;"> *</span></label>
                                                            <input type="number" min="0" name="nik" class="form-control" value="{{ old('nik') }}" placeholder="NIK" required>
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('nik'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('nik')}}
                                                                @endif
                                                            </p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Jenis Kelamin<span style="color:red;"> *</span></label>
                                                            <select class="form-control" name="jenis_kelamin" required>
                                                                <option selected="true" disabled="disabled" value="{{ old('jenis_kelamin') }}">Pilih</option>
                                                                <option value="Laki-Laki">Laki-Laki</option>
                                                                <option value="Perempuan">Perempuan</option>
                                                            </select>
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('jenis_kelamin'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('jenis_kelamin')}}
                                                                @endif
                                                            </p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Tanggal Lahir<span style="color:red;"> *</span></label>
                                                            <input type="date" name="tanggal_lahir" class="form-control" value="{{ old('tanggal_lahir') }}" placeholder="Tanggal Lahir" required>
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('tanggal_lahir'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('tanggal_lahir')}}
                                                                @endif
                                                            </p>
                                                        </div><br /><br />
                                                        <div class="form-group">
                                                            <label>NIK ortu/wali</label>
                                                            <input type="number" min="0" name="nik_ortu" class="form-control" value="{{ old('nik_ortu') }}" placeholder="NIK ortu/wali">
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('nik_ortu'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('nik_ortu')}}
                                                                @endif
                                                            </p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>No HP ortu/wali</label>
                                                            <input type="number" min="0" name="no_hp_ortu" class="form-control" value="{{ old('no_hp_ortu') }}" placeholder="No Hp ortu/wali">
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('no_hp_ortu'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('no_hp_ortu')}}
                                                                @endif
                                                            </p>
                                                        </div>

                                                        <label>Foto Anak<span style="color:red;"> *</span></label>
                                                        <div class="form-group dropzone">
                                                            <input class="form-control" name="foto_anak" type="file" value="{{ old('foto_anak') }}" required="required" multiple />
                                                        </div>
                                                        <p style="color:red;font-size:13px;">
                                                            @if($errors->has('foto_anak'))
                                                            <i class="fa fa-exclamation-circle"></i> {{$errors->first('foto_anak')}}
                                                            @endif
                                                        </p>

                                                    </div>

                                                    <!--- Rows Kanan -->
                                                    <div class="col-lg-6">
                                                        @if(Auth::user()->role == "superadmin")
                                                        <div class="form-group">
                                                            <label>Nama Yayasan<span style="color:red;"> *</span></label>
                                                            <select class="form-control" name="id_yayasan" required>
                                                                @if(Auth::user()->role == "superadmin")
                                                                <option selected="true" disabled="disabled" value="{{ old('id_yayasan') }}">Pilih</option>
                                                                @endif

                                                                @foreach($yayasan as $data)
                                                                <option value="{{ $data->id }}">{{ $data->nama_yayasan }}</option>
                                                                @endforeach
                                                            </select>
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('id_yayasan'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('id_yayasan')}}
                                                                @endif
                                                            </p>
                                                        </div>
                                                        @else
                                                        <input type="hidden" name="id_yayasan" value="{{ Auth::user()->id_yayasan }}">
                                                        @endif

                                                        <div class="form-group">
                                                            <label>Nama Anak<span style="color:red;"> *</span></label>
                                                            <input type="text" name="nama_anak" class="form-control" value="{{ old('nama_anak') }}" placeholder="Nama Anak" required>
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('nama_anak'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('nama_anak')}}
                                                                @endif
                                                            </p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Provinsi Lahir<span style="color:red;"> (sesuai akte)*</span></label>
                                                            <!-- <input type="text" name="tempat_lahir" class="form-control" value="{{ old('tempat_lahir') }}" placeholder="Tempat Lahir" required> -->
                                                            <select class="js-example-basic-single form-control " name="tempat_lahir" style="width:100%;" required>
                                                                <option selected="true" disabled="disabled" value="{{ old('id_anak') }}">Pilih</option>
                                                                <option value="Nanggroe Aceh Darussalam">Nanggroe Aceh Darussalam</option>
                                                                <option value="Sumatera Utara">Sumatera Utara</option>
                                                                <option value="Sumatera Barat">Sumatera Barat</option>
                                                                <option value="Riau">Riau</option>
                                                                <option value="Kepulauan Riau">Kepulauan Riau</option>
                                                                <option value="Jambi">Jambi</option>
                                                                <option value="Bengkulu">Bengkulu</option>
                                                                <option value="Sumatera Selatan">Sumatera Selatan</option>
                                                                <option value="Kepulauan Bangka Belitung">Kepulauan Bangka Belitung</option>
                                                                <option value="Lampung">Lampung</option>
                                                                <option value="Banten">Banten</option>
                                                                <option value="Jawa Barat">Jawa Barat</option>
                                                                <option value="DKI Jakarta">DKI Jakarta</option>
                                                                <option value="Jawa Tengah">Jawa Tengah</option>
                                                                <option value="DI Yogyakarta">DI Yogyakarta</option>
                                                                <option value="Jawa Timur">Jawa Timur</option>
                                                                <option value="Bali">Bali</option>
                                                                <option value="Nusa Tenggara Barat">Nusa Tenggara Barat</option>
                                                                <option value="Nusa Tenggara Timur">Nusa Tenggara Timur</option>
                                                                <option value="Kalimantan Utara">Kalimantan Utara</option>
                                                                <option value="Kalimantan Barat">Kalimantan Barat</option>
                                                                <option value="Kalimantan Tengah">Kalimantan Tengah</option>
                                                                <option value="Kalimantan Selatan">Kalimantan Selatan</option>
                                                                <option value="Kalimantan Timur">Kalimantan Timur</option>
                                                                <option value="Gorontalo">Gorontalo</option>
                                                                <option value="Sulawesi Utara">Sulawesi Utara</option>
                                                                <option value="Sulawesi Barat">Sulawesi Barat</option>
                                                                <option value="Sulawesi Tengah">Sulawesi Tengah</option>
                                                                <option value="Sulawesi Selatan">Sulawesi Selatan</option>
                                                                <option value="Sulawesi Tenggara">Sulawesi Tenggara</option>
                                                                <option value="Maluku Utara">Maluku Utara</option>
                                                                <option value="Maluku">Maluku</option>
                                                                <option value="Papua Barat">Papua Barat</option>
                                                                <option value="Papua">Papua</option>
                                                            </select>
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('tempat_lahir'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('tempat_lahir')}}
                                                                @endif
                                                            </p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Kota Lahir</label>
                                                            <input type="text" name="kota_lahir" class="form-control" value="{{ old('kota_lahir') }}" placeholder="Kota">
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('kota_lahir'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('kota_lahir')}}
                                                                @endif
                                                            </p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Alamat Tempat Lahir<span style="color:red;"> (sesuai akte)*</span></label>
                                                            <textarea style="border-color:lightgrey;" name="alamat" required></textarea>
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('alamat'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('alamat')}}
                                                                @endif
                                                            </p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Nama ortu/wali</label>
                                                            <input type="text" name="nama_ortu" class="form-control" value="{{ old('nama_ortu') }}" placeholder="Nama ortu/wali">
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('nama_ortu'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('nama_ortu')}}
                                                                @endif
                                                            </p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Alamat ortu/wali</label>
                                                            <textarea style="border-color:lightgrey;" name="alamat_ortu"></textarea>
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('alamat_ortu'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('alamat_ortu')}}
                                                                @endif
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="data-fisik" role="tabpanel">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="basic-elements">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <div class="form-group row m-l-2">
                                                            <div class="col-xs-2">
                                                                <label>Golongan Darah</label>
                                                                <!-- <input type="text" name="gol_darah" class="form-control col-xs-2" value="{{ old('gol_darah') }}" style="text-transform:uppercase;" placeholder="Gol Darah"> -->
                                                                <select name="gol_darah" class="form-control" id="">
                                                                    <option disabled selected>Pilih</option>
                                                                    <option value="A">A</option>
                                                                    <option value="B">B</option>
                                                                    <option value="AB">AB</option>
                                                                    <option value="O">O</option>
                                                                </select>
                                                                <p style="color:red;font-size:13px;">
                                                                    @if($errors->has('gol_darah'))
                                                                    <i class="fa fa-exclamation-circle"></i> {{$errors->first('gol_darah')}}
                                                                    @endif
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Suhu Badan</label>
                                                            <input type="number" min="0" name="suhu_badan" class="form-control" value="{{ old('suhu_badan') }}" placeholder="(dalam derajat Celcius)">
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('suhu_badan'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('suhu_badan')}}
                                                                @endif
                                                            </p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Tekanan Darah</label>
                                                            <input type="number" min="0" name="tekanan_darah" class="form-control" value="{{ old('tekanan_darah') }}" placeholder="(dalam mm/Hg)">
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('tekanan_darah'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('tekanan_darah')}}
                                                                @endif
                                                            </p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Berat Badan</label>
                                                            <input type="number" min="0" name="berat_badan" class="form-control" value="{{ old('berat_badan') }}" placeholder="(dalam Kg)">
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('berat_badan'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('berat_badan')}}
                                                                @endif
                                                            </p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Tinggi Badan</label>
                                                            <input type="number" min="0" name="tinggi_badan" class="form-control" value="{{ old('tinggi_badan') }}" placeholder="(dalam Cm)">
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('tinggi_badan'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('tinggi_badan')}}
                                                                @endif
                                                            </p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Indeks Massa Tubuh</label>
                                                            <input type="number" min="0" name="massa_tubuh" class="form-control" value="{{ old('massa_tubuh') }}" placeholder="(dalam kg/m2)">
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('massa_tubuh'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('massa_tubuh')}}
                                                                @endif
                                                            </p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Disabilitas<span style="color:red;"> *</span></label>
                                                            <select class="js-example-basic-single form-control" name="disabilitas" style="width:100%;height:50%;" required>
                                                                <option selected="true" disabled="disabled">Pilih</option>
                                                                <option value="Buta (Tuna Netra)">Buta (Tuna Netra)</option>
                                                                <option value="Tuli (Tuna Rungu)">Tuli (Tuna Rungu)</option>
                                                                <option value="Bisu (Tuna Wicara)">Bisu (Tuna Wicara)</option>
                                                                <option value="Cacat Fisik (Tuna Daksa)">Cacat Fisik (Tuna Daksa)</option>
                                                                <option value="Keterbelakangan Mental (Tuna Grahita)">Keterbelakangan Mental (Tuna Grahita)</option>
                                                                <option value="Cacat Pengendalian Diri (Tuna Laras)">Cacat Pengendalian Diri (Tuna Laras)</option>
                                                                <option value="Cacat Kombinasi (Tuna Ganda)">Cacat Kombinasi (Tuna Ganda)</option>
                                                            </select>
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('disabilitas'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('disabilitas')}}
                                                                @endif
                                                            </p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Alergi</label>
                                                            <input type="text" name="alergi" class="form-control" value="{{ old('alergi') }}" placeholder="Alergi">
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('alergi'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('alergi')}}
                                                                @endif
                                                            </p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Catatan / Saran</label>
                                                            <textarea style="border-color:lightgrey;" name="catatan"> </textarea>
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('catatan'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('catatan')}}
                                                                @endif
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group row m-l-2 m-b-35">
                                                            <div class="col-xs-2">
                                                                <label>Rhesus</label><br>
                                                                <input class="yes" type="radio" name="rhesus" value="+" required> + &nbsp;&nbsp;
                                                                <input class="no" type="radio" name="rhesus" value="-"> -
                                                                <p style="color:red;font-size:13px;">
                                                                    @if($errors->has('tgl_suhu_badan'))
                                                                    <i class="fa fa-exclamation-circle"></i> {{$errors->first('tgl_suhu_badan')}}
                                                                    @endif
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>&nbsp;</label>
                                                            <input type="date" name="tgl_suhu_badan" class="form-control" value="{{ old('tgl_suhu_badan') }}" placeholder="Terakhir Periksa">
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('tgl_suhu_badan'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('tgl_suhu_badan')}}
                                                                @endif
                                                            </p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>&nbsp;</label>
                                                            <input type="date" name="tgl_tekanan_darah" class="form-control" value="{{ old('tgl_tekanan_darah') }}" placeholder="Terakhir Periksa">
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('tgl_tekanan_darah'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('tgl_tekanan_darah')}}
                                                                @endif
                                                            </p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>&nbsp;</label>
                                                            <input type="date" name="tgl_berat_badan" class="form-control" value="{{ old('tgl_berat_badan') }}" placeholder="Terakhir Periksa">
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('tgl_berat_badan'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('tgl_berat_badan')}}
                                                                @endif
                                                            </p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>&nbsp;</label>
                                                            <input type="date" name="tgl_tinggi_badan" class="form-control" value="{{ old('tgl_tinggi_badan') }}" placeholder="Terakhir Periksa">
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('tgl_tinggi_badan'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('tgl_tinggi_badan')}}
                                                                @endif
                                                            </p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>&nbsp;</label>
                                                            <input type="date" name="tgl_massa_tubuh" class="form-control" value="{{ old('tgl_massa_tubuh') }}" placeholder="Terakhir Periksa">
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('tgl_massa_tubuh'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('tgl_massa_tubuh')}}
                                                                @endif
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="data-document" role="tabpanel">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="basic-elements">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <label>Akte Lahir<span style="color:red;"> *</span></label>
                                                        <div class="form-group dropzone">
                                                            <input class="form-control" name="foto_akte_lahir" type="file" value="{{ old('foto_akte_lahir') }}" required="required" multiple />
                                                        </div>
                                                        <p style="color:red;font-size:13px;">
                                                            @if($errors->has('foto_akte_lahir'))
                                                            <i class="fa fa-exclamation-circle"></i> {{$errors->first('foto_akte_lahir')}}
                                                            @endif
                                                        </p>
                                                        <label>Kartu Keluarga</label>
                                                        <div class="form-group dropzone">
                                                            <input class="form-control" name="foto_kartu_keluarga" type="file" value="{{ old('foto_kartu_keluarga') }}" multiple />
                                                        </div>
                                                        <p style="color:red;font-size:13px;">
                                                            @if($errors->has('foto_kartu_keluarga'))
                                                            <i class="fa fa-exclamation-circle"></i> {{$errors->first('foto_kartu_keluarga')}}
                                                            @endif
                                                        </p>
                                                        <label>Kartu BPJS</label>
                                                        <div class="form-group dropzone">
                                                            <input class="form-control" name="foto_kartu_bpjs" type="file" value="{{ old('foto_kartu_bpjs') }}" multiple />
                                                        </div>
                                                        <p style="color:red;font-size:13px;">
                                                            @if($errors->has('foto_kartu_bpjs'))
                                                            <i class="fa fa-exclamation-circle"></i> {{$errors->first('foto_kartu_bpjs')}}
                                                            @endif
                                                        </p>
                                                        <label>KTP</label>
                                                        <div class="form-group dropzone">
                                                            <input class="form-control" name="foto_ktp" type="file" value="{{ old('foto_ktp') }}" multiple />
                                                        </div>
                                                        <p style="color:red;font-size:13px;">
                                                            @if($errors->has('foto_ktp'))
                                                            <i class="fa fa-exclamation-circle"></i> {{$errors->first('foto_ktp')}}
                                                            @endif
                                                        </p>
                                                        <label>Foto Rontgen<span style="color:red;"> (apabila pernah diagnosa)</span></label>
                                                        <div class="form-group dropzone">
                                                            <input class="form-control" name="foto_rontgen" type="file" value="{{ old('foto_rontgen') }}" multiple />
                                                        </div>
                                                        <p style="color:red;font-size:13px;">
                                                            @if($errors->has('foto_rontgen'))
                                                            <i class="fa fa-exclamation-circle"></i> {{$errors->first('foto_rontgen')}}
                                                            @endif
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- 
                        <div class="tab-pane" id="kesehatan" role="tabpanel">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="basic-elements">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        
                                                        <label>Nama Penyakit<span style="color:red;"> *</span></label>
                                                        <div class="form-group">
                                                            <select class="form-control js-example-basic-single" name="nama_penyakit[]" multiple="multiple" required>
                                                                <option value="Kanker">Kanker</option>
                                                                <option value="Flu">Flu</option>
                                                                <option value="Batuk">Batuk</option>
                                                                <option value="Pilek">Pilek</option>
                                                                <option value="Panas">Panas</option>
                                                                <option value="Rindu">Rindu</option>
                                                                <option value="Kangen">Kangen</option>
                                                                <option value="Bertemu">Bertemu</option>
                                                                <option value="Oke">Oke</option>
                                                            </select>
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('id_yayasan'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('id_yayasan')}}
                                                                @endif
                                                            </p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Gejala penyakit<span style="color:red;"> *</span></label>
                                                            <textarea style="border-color:lightgrey;" name="gejala_penyakit" rows="3" cols="58" required></textarea>
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('gejala_penyakit'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('gejala_penyakit')}}
                                                                @endif
                                                            </p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Catatan Dokter<span style="color:red;"> *</span></label>
                                                            <textarea style="border-color:lightgrey;" name="catatan_dokter" rows="3" cols="58" required></textarea>
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('catatan_dokter'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('catatan_dokter')}}
                                                                @endif
                                                            </p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Terima Obat<span style="color:red;"> *</span></label><br>
                                                            <input class="yes" type="radio" name="terima_obat" value="Ya" required> Ya &nbsp;&nbsp;
                                                            <input class="no" type="radio" name="terima_obat" value="Tidak"> Tidak
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('terima_obat'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('terima_obat')}}
                                                                @endif
                                                            </p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Obat/Resep<span style="color:red;"> *</span></label>
                                                            <textarea class="resep_obat" style="border-color:lightgrey;background-color:lightgrey;" name="obat_resep" rows="3" cols="58" required="required" readonly></textarea>
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('obat_resep'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('obat_resep')}}
                                                                @endif
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->

                        <div class="dt-buttons">
                            <div class="sweetalert m-t-15">
                                <button class="btn btn-info btn sweet-success" type="submit">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection