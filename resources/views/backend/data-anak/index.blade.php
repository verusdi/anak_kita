@extends('backend.layouts.app')

@section('content')
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Data Anak</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Data Anak</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <a href="{{ route('data-anak.create')}}" class="btn btn-info" name="button">Tambah Data</a>
                        <div class="table-responsive m-t-0">
                            <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>NIK</th>
                                        <th>Nama</th>
                                        <th>Jenis Kelamin</th>
                                        <th>Tempat Lahir</th>
                                        <th>Tanggal Lahir</th>
                                        <th>Yayasan</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($datas as $data)
                                    <tr>
                                        <td style="text-decoration:underline;"><a href="{{ url('/data-anak',$data->id) }}">{{ $data->kode_anak }}</a></td>
                                        <td>{{ $data->nama_anak }}</td>
                                        <td>{{ $data->jenis_kelamin }}</td>
                                        <td>{{ $data->tempat_lahir }}</td>
                                        <td>{{ $data->tanggal_lahir }}</td>
                                        <td>{{ $data->yayasan->nama_yayasan }}</td>
                                        <td style="display:inline-flex;margin-left:15%;">
                                            <a href="{{ route('data-anak.edit', $data->id) }}" class="btn btn-warning" name="button"><i class="fa fa-pencil" aria-hidden="true"> </i></a>&nbsp;
                                            <!-- <button class="btn btn-danger btn sweet-confirm" form="{{ $data->id }}"><i class="fa fa-trash-o" aria-hidden="true"> </i></button> -->
                                            <form action="{{ route('data-anak.destroy',$data->id) }}" onsubmit="return confirm('Yakin ingin menghapus data ini?');" method="POST">
                                                <input type="hidden" name="_method" value="DELETE">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <button type="submit" class="btn btn-danger btn sweet-confirm"><i class="fa fa-trash-o" aria-hidden="true"> </i></button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End PAge Content -->
</div>
@endsection