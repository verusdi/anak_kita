@extends('backend.layouts.app')

@section('content')
<style>
    .form-control::-webkit-input-placeholder {
        font-size: 14px;
        color: darkgrey;
    }

    .form-control::-moz-input-placeholder {
        color: darkgrey;
    }

    .form-control:-ms-input-placeholder {
        color: darkgrey;
    }

    textarea {
        width: 100%;
        height: 110px;
    }
</style>
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Vaksinasi</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Tambah</a></li>
                <li class="breadcrumb-item active">Vaksinasi</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <!-- /# row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-title">
                        <h4>Tambah vaksinasi</h4>

                    </div>
                    <div class="card-body">
                        <div class="basic-elements">
                            <form action="{{ route('riwayat-vaksinasi.store') }}" enctype="multipart/form-data" method="post">
                                @csrf
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Nama Anak<span style="color:red;"> *</span></label>
                                            <select class="js-example-basic-single form-control " name="id_anak" style="width:100%;" required>
                                                <option selected="true" disabled="disabled" value="{{ old('id_anak') }}">Pilih</option>
                                                @foreach($datas as $data)
                                                <option value="{{ $data->id }}">{{ $data->nama_anak }}</option>
                                                @endforeach
                                            </select>
                                            <p style="color:red;font-size:13px;">
                                                @if($errors->has('id_anak'))
                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('id_anak')}}
                                                @endif
                                            </p>
                                        </div>

                                        @if(Auth::user()->role == "superadmin")
                                        <div class="form-group">
                                            <label>Nama Yayasan<span style="color:red;"> *</span></label>
                                            <select class="js-example-basic-single form-control" name="id_yayasan" style="width:100%;" required>
                                                <option selected="true" disabled="disabled" value="{{ old('id_yayasan') }}">Pilih</option>
                                                @foreach($yayasan as $data)
                                                <option value="{{ $data->id }}">{{ $data->nama_yayasan }}</option>
                                                @endforeach
                                            </select>
                                            <p style="color:red;font-size:13px;">
                                                @if($errors->has('id_yayasan'))
                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('id_yayasan')}}
                                                @endif
                                            </p>
                                        </div>
                                        @else
                                        <input type="hidden" name="id_yayasan" value="{{ Auth::user()->id_yayasan }}">
                                        @endif

                                        <div class="form-group">
                                            <label>Pemberi Vaksin<span style="color:red;"> *</span></label>
                                            <input type="text" name="pemberi_vaksinasi" class="form-control" value="{{ old('pemberi_vaksinasi') }}" placeholder="Pemberi Vaksin" required>
                                            <p style="color:red;font-size:13px;">
                                                @if($errors->has('pemberi_vaksinasi'))
                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('pemberi_vaksinasi')}}
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Posyandu/Rumah Sakit<span style="color:red;"> *</span></label>
                                            <input type="text" name="tempat_vaksinasi" class="form-control" value="{{ old('tempat_vaksinasi') }}" placeholder="Nama Tempat" required>
                                            <p style="color:red;font-size:13px;">
                                                @if($errors->has('tempat_vaksinasi'))
                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('tempat_vaksinasi')}}
                                                @endif
                                            </p>
                                        </div>
                                        <div class="form-group">
                                            <label>Alamat tempat di vaksin<span style="color:red;"> *</span></label>
                                            <textarea style="border-color:lightgrey;" name="alamat_tempat_vaksinasi" required></textarea>
                                            <p style="color:red;font-size:13px;">
                                                @if($errors->has('alamat_tempat_vaksinasi'))
                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('alamat_tempat_vaksinasi')}}
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <hr class="style">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Jenis Vaksinasi<span style="color:red;"> *</span></label><br>
                                            <input id="wajib" type="radio" name="jenis_vaksinasi" value="Wajib" required> Wajib &nbsp;
                                            <input id="tambahan" type="radio" name="jenis_vaksinasi" value="Tambahan"> Tambahan
                                        </div>
                                        <div class="form-group">
                                            <label>Vaksinasi Wajib</label>
                                            <select id="vaksinasi_wajib" class="js-example-basic-single form-control" name="vaksinasi_wajib" disabled>
                                                <option value="" selected disabled>Pilih</option>
                                                <option value="Hepatitis B">Hepatitis B</option>
                                                <option value="Polio">Polio</option>
                                                <option value="BCG">BCG</option>
                                                <option value="Campak">Campak</option>
                                                <option value="Pentavalen">Pentavalen (DPT-HB-HiB)</option>
                                            </select>
                                            <p style="color:red;font-size:13px;">
                                                @if($errors->has('id_yayasan'))
                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('id_yayasan')}}
                                                @endif
                                            </p>
                                        </div>
                                        <div class="form-group">
                                            <label>Vaksinasi Tambahan</label>
                                            <select id="vaksinasi_tambahan" class="js-example-basic-single form-control" name="vaksinasi_tambahan" disabled>
                                                <option value="" selected disabled>Pilih</option>
                                                <option value="Pneumokokus Conjugate Vaccine">Pneumokokus Conjugate Vaccine(PCV)</option>
                                                <option value="Varisela">Varisela</option>
                                                <option value="Influenza">Influenza</option>
                                                <option value="Hepatitis A">Hepatitis A</option>
                                                <option value="HPV">HPV (Human Papiloma Virus)</option>
                                            </select>
                                            <p style="color:red;font-size:13px;">
                                                @if($errors->has('id_yayasan'))
                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('id_yayasan')}}
                                                @endif
                                            </p>
                                        </div>
                                        <div class="form-group">
                                            <label>Catatan</label>
                                            <textarea style="border-color:lightgrey;" name="catatan"></textarea>
                                            <p style="color:red;font-size:13px;">
                                                @if($errors->has('catatan'))
                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('catatan')}}
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <input type="hidden" name="" class="form-control" value="" placeholder="">
                                        </div><br><br>
                                        <div class="form-group">
                                            <label>&nbsp;</label>
                                            <input type="date" id="tgl_vaksinasi_wajib" name="tgl_vaksinasi_wajib" class="form-control" value="{{ old('tgl_vaksinasi_wajib') }}" placeholder="Terakhir Periksa" disabled>
                                            <p style="color:red;font-size:13px;">
                                                @if($errors->has('tgl_vaksinasi_wajib'))
                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('tgl_vaksinasi_wajib')}}
                                                @endif
                                            </p>
                                        </div><br>
                                        <div class="form-group">
                                            <input type="date" id="tgl_vaksinasi_tambahan" name="tgl_vaksinasi_tambahan" class="form-control" value="{{ old('tgl_vaksinasi_tambahan') }}" placeholder="Terakhir Periksa" disabled>
                                            <p style="color:red;font-size:13px;">
                                                @if($errors->has('tgl_vaksinasi_tambahan'))
                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('tgl_vaksinasi_tambahan')}}
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="dt-buttons">
                                    <div class="sweetalert m-t-15">
                                        <button class="btn btn-info btn sweet-success" name="save" id="save" type="submit">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
</div>
@endsection