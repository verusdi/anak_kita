@extends('backend.layouts.app')

@section('content')
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Data Yayasan</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Data Yayasan</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive m-t-0">
                            <a href="{{ route('yayasan.create')}}" class="btn btn-info" name="button">Tambah Yayasan</a>
                            <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Nama Yayasan</th>
                                        <th>Foto Yayasan</th>
                                        <th>No Telp</th>
                                        <th>Tgl Berdiri</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($datas as $data)
                                    <tr>
                                        <td style="text-decoration:underline;"><a href="{{ route('yayasan.show',$data->id) }}"> {{ $data->nama_yayasan }}</a></td>
                                        <td>
                                            @if($data->foto_yayasan)
                                            <img src="{{asset('images/'.$data->foto_yayasan)}}" alt="logo" height="50" width="50" style="display:block;margin-left:auto;margin-right:auto;">
                                            @else
                                            <p>Tidak ada gambar</p>
                                            @endif
                                        </td>
                                        <td>{{ $data->no_telp }}</td>
                                        <td>{{ $data->tgl_berdiri }}</td>
                                        <td style="display:inline-flex;margin-left:17%;">
                                            <a href="{{ route('yayasan.edit', $data->id) }}" class="btn btn-warning" name="button"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;
                                            <!-- <button class="btn btn-danger btn sweet-confirm" form="{{ $data->id }}"><i class="fa fa-trash-o" aria-hidden="true"> </i></button> -->
                                            <form action="{{ route('yayasan.destroy',$data->id) }}" onsubmit="return confirm('Yakin ingin menghapus data ini?');" method="POST">
                                                <input type="hidden" name="_method" value="DELETE">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <button type="submit" class="btn btn-danger btn sweet-confirm"><i class="fa fa-trash-o" aria-hidden="true"> </i></button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
</div>
@endsection