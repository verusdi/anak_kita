@extends('backend.layouts.app')

@section('content')
<style>
    .form-control::-webkit-input-placeholder {
        font-size: 14px;
        color: darkgrey;
    }

    .form-control::-moz-input-placeholder {
        color: darkgrey;
    }

    .form-control:-ms-input-placeholder {
        color: darkgrey;
    }

    textarea {
        width: 100%;
        height: 80px;
    }
</style>
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Kesehatan</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Tambah</a></li>
                <li class="breadcrumb-item active">Kesehatan</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <!-- /# row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-title">
                        <h4></h4>


                    </div>
                    <div class="card-body">
                        <div class="basic-elements">
                            <form action="{{ route('riwayat-kesehatan.update',$kesehatan->id) }}" enctype="multipart/form-data" method="post">
                                @csrf
                                {{ method_field('PUT') }}
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Nama Anak<span style="color:red;"> *</span></label>
                                            <select class="js-example-basic-single form-control " name="id_anak" value="{{ $kesehatan->id_anak }}">
                                                <option selected="true" disabled="disabled" value="{{ $kesehatan->id_anak }}">{{$kesehatan->dataAnak->nama_anak}}</option>
                                                @foreach($datas as $data)
                                                <option value="{{ $data->id }}">{{ $data->nama_anak }}</option>
                                                @endforeach
                                            </select>
                                            <p style="color:red;font-size:13px;">
                                                @if($errors->has('id_anak'))
                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('id_anak')}}
                                                @endif
                                            </p>
                                        </div>
                                        <div class="form-group">
                                            <label>Nama Yayasan<span style="color:red;"> *</span></label>
                                            <select class="js-example-basic-single form-control" name="id_yayasan" value="{{ $kesehatan->id_yayasan }}">
                                                @if(Auth::user()->role == " superadmin")
                                                <option selected="true" disabled="disabled" value="{{ $kesehatan->id_yayasan }}">{{ $kesehatan->dataYayasan->nama_yayasan }}</option>
                                                @endif
                                                @foreach($yayasan as $data)
                                                <option value="{{ $data->id }}">{{ $data->nama_yayasan }}</option>
                                                @endforeach
                                            </select>
                                            <p style="color:red;font-size:13px;">
                                                @if($errors->has('id_yayasan'))
                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('id_yayasan')}}
                                                @endif
                                            </p>
                                        </div>
                                        <!-- <div class="form-group">
                                            <label>Nama Penyakit<span style="color:red;"> *</span></label>
                                            <input type="text" name="nama_penyakit" class="form-control" value="{{ $kesehatan->nama_penyakit }}" placeholder="Nama Penyakit" required>

                                            <p style="color:red;font-size:13px;">
                                                @if($errors->has('nama_penyakit'))
                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('nama_penyakit')}}
                                                @endif
                                            </p>
                                        </div> -->
                                        <div class="form-group">
                                            <label>Nama Penyakit<span style="color:red;"> *</span></label>
                                            <select class="js-example-basic-single form-control" name="nama_penyakit[]" multiple="multiple">
                                                @php
                                                $datas = explode(', ',$kesehatan->nama_penyakit);
                                                @endphp

                                                @foreach($datas as $key=>$data)
                                                <option selected="true" value="{{ $data }}">{{ $data }}</option>
                                                @endforeach

                                                @foreach($penyakit as $value)
                                                <option value="{{ $value->nama_penyakit }}">{{ $value->nama_penyakit }}</option>
                                                @endforeach
                                            </select>
                                            <p style="color:red;font-size:13px;">
                                                @if($errors->has('id_yayasan'))
                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('id_yayasan')}}
                                                @endif
                                            </p>
                                        </div>
                                        <div class="form-group">
                                            <label>Gejala penyakit<span style="color:red;"> *</span></label>
                                            <textarea style="border-color:lightgrey;" name="gejala_penyakit" rows="3" cols="58" required>{{ $kesehatan->gejala_penyakit }}</textarea>
                                            <p style="color:red;font-size:13px;">
                                                @if($errors->has('gejala_penyakit'))
                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('gejala_penyakit')}}
                                                @endif
                                            </p>
                                        </div>
                                        <div class="form-group">
                                            <label>Catatan Dokter<span style="color:red;"> *</span></label>
                                            <textarea style="border-color:lightgrey;" name="catatan_dokter" rows="3" cols="58" required>{{ $kesehatan->catatan_dokter }}</textarea>
                                            <p style="color:red;font-size:13px;">
                                                @if($errors->has('catatan_dokter'))
                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('catatan_dokter')}}
                                                @endif
                                            </p>
                                        </div>
                                        <div class="form-group">
                                            <label>Terima Obat<span style="color:red;"> *</span></label><br>
                                            @if($kesehatan->terima_obat == "Ya")
                                            <input class="yes" type="radio" name="terima_obat" value="{{ $kesehatan->terima_obat }}" checked> Ya &nbsp;&nbsp;
                                            <input class="no" type="radio" name="terima_obat" value="Tidak"> Tidak
                                            @elseif($kesehatan->terima_obat == "Tidak")
                                            <input class="yes" type="radio" name="terima_obat" value="Ya"> Ya &nbsp;&nbsp;
                                            <input class="no" type="radio" name="terima_obat" value="{{ $kesehatan->terima_obat }}" checked> Tidak
                                            @else
                                            <input class="yes" type="radio" name="terima_obat" value="Ya"> Ya &nbsp;&nbsp;
                                            <input class="no" type="radio" name="terima_obat" value="Tidak"> Tidak
                                            @endif
                                            <p style="color:red;font-size:13px;">
                                                @if($errors->has('terima_obat'))
                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('terima_obat')}}
                                                @endif
                                            </p>
                                        </div>
                                        <div class="form-group">
                                            <label>Obat/Resep<span style="color:red;"> *</span></label>
                                            <textarea class="resep_obat" style="border-color:lightgrey;background-color:lightgrey;" name="obat_resep" rows="3" cols="58" required="required" readonly>{{ $kesehatan->obat_resep }}</textarea>
                                            <p style="color:red;font-size:13px;">
                                                @if($errors->has('obat_resep'))
                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('obat_resep')}}
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="dt-buttons">
                                    <div class="sweetalert m-t-15">
                                        <button class="btn btn-info btn sweet-success" type="submit">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
</div>
@endsection