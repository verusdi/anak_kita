@extends('backend.layouts.app')

@section('content')
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Data kesehatan</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Data Kesehatan</a></li>
                <li class="breadcrumb-item active">Show</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <!-- /# row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card pink">
                    <div class="card-title">
                        <h2 style="text-decoration:underline;" class="pink-name">Info Lengkap Kesehatan</h2>
                        <a href="{{ route('export.kesehatan',$kesehatan->id) }}" class="btn float-right" name="button" style="margin-left:1%;background-color:white;color:#e9748a;">Excel</a>
                        <a href="{{ route('pdf.kesehatan',$kesehatan->id) }}" class="btn float-right" name="button" style="background-color:white;color:#e9748a;">PDF</a>
                    </div>
                    <div class="card-body">
                        <div class="basic-elements">
                            <div class="card">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group" style="margin-bottom:10%;">
                                            <label style="font-weight:bold;">Nama:</label><br>
                                            <h4>{{ $kesehatan->dataAnak->nama_anak }}</h4>
                                        </div>
                                        <div class="form-group" style="margin-bottom:10%;">
                                            <label style="font-weight:bold;">Catatan Dokter:</label><br>
                                            <h4>{{ $kesehatan->catatan_dokter }}</h4>
                                        </div>
                                        <div class="form-group" style="margin-bottom:10%;">
                                            <label style="font-weight:bold;">Terima Obat:</label><br>
                                            <h4>{{ $kesehatan->terima_obat }}</h4>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group" style="margin-bottom:10%;">
                                            <label style="font-weight:bold;">Penyakit:</label><br>
                                            <h4>{{ $kesehatan->nama_penyakit }}</h4>
                                        </div>
                                        <div class="form-group" style="margin-bottom:10%;">
                                            <label style="font-weight:bold;">Tgl Pengobatan:</label><br>
                                            <h4>{{ $kesehatan->tanggal_pengobatan }}</h4>
                                        </div>
                                        <div class="form-group" style="margin-bottom:10%;">
                                            <label style="font-weight:bold;">Gelaja Penyakit:</label><br>
                                            <h4>{{ $kesehatan->gejala_penyakit }}</h4>
                                        </div>
                                        <div class="form-group" style="margin-bottom:10%;">
                                            <label style="font-weight:bold;">Obat/Resep Dokter:</label><br>
                                            <h4>{{ $kesehatan->obat_resep }}</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="dt-buttons">
                                    <div class="sweetalert m-t-15">
                                        <a href="{{ route('riwayat-kesehatan.edit', $kesehatan->id) }}" class="btn btn-warning btn-md m-b-10 m-l-5" name="button">Edit</a>
                                        <a href="{{ route('riwayat-kesehatan.index') }}" class="btn btn-primary btn-md m-b-10 m-l-5" name="button">Back</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
</div>
@endsection
