@extends('backend.layouts.app')

@section('content')
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Data User Admin</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Data User Admin</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive m-t-0">
                            <a href="{{ route('user.create')}}" class="btn btn-info" name="button">Tambah Data User</a>
                            <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>Role</th>
                                        <th>Nama Yayasan</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($user as $data)
                                    <tr>
                                        <td>{{ $data->name }}</td>
                                        <td>{{ $data->email }}</td>
                                        <td>{{ $data->role }}</td>
                                        <td>{{ $data->dataYayasan->nama_yayasan }}</td>
                                        <td style="display:inline-flex;margin-left:15%;">
                                            <a href="{{ route('user.edit', $data->id) }}" class="btn btn-warning" name="button"><i class="fa fa-pencil" aria-hidden="true"> </i></a>&nbsp;
                                            <!-- <button class="btn btn-danger btn sweet-confirm" form="{{ $data->id }}"><i class="fa fa-trash-o" aria-hidden="true"> </i></button> -->
                                            <form action="{{ route('user.destroy',$data->id) }}" onsubmit="return confirm('Yakin ingin menghapus data ini?');" method="POST">
                                                <input type="hidden" name="_method" value="DELETE">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <button type="submit" class="btn btn-danger btn sweet-confirm"><i class="fa fa-trash-o" aria-hidden="true"> </i></button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
</div>
@endsection