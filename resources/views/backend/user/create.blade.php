@extends('backend.layouts.app')

@section('content')
<style>
    .form-control::-webkit-input-placeholder {
        font-size: 14px;
        color: darkgrey;
    }

    .form-control::-moz-input-placeholder {
        color: darkgrey;
    }

    .form-control:-ms-input-placeholder {
        color: darkgrey;
    }
</style>
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">User</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Tambah</a></li>
                <li class="breadcrumb-item active">User</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <!-- /# row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-title">
                        <h4>Tambah User</h4>
                        @if (session('error'))
                        <div class="alert alert-danger">{{ session('error') }}</div>
                        @endif

                    </div>
                    <div class="card-body">
                        <div class="basic-elements">
                            <form action="{{ route('user.store') }}" enctype="multipart/form-data" method="post">
                                @csrf
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Nama<span style="color:red;"> *</span></label>
                                            <input type="text" name="name" class="form-control" value="{{ old('name') }}" placeholder="Nama" required>
                                            <p style="color:red;font-size:13px;">
                                                @if($errors->has('name'))
                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('name')}}
                                                @endif
                                            </p>
                                        </div>
                                        <div class="form-group">
                                            <label>Email<span style="color:red;"> *</span></label>
                                            <input type="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="Email" required>
                                            <p style="color:red;font-size:13px;">
                                                @if($errors->has('email'))
                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('email')}}
                                                @endif
                                            </p>
                                        </div>
                                        <div class="form-group">
                                            <label>Password<span style="color:red;"> *</span></label>
                                            <input type="password" name="password" class="form-control" value="{{ old('password') }}" placeholder="Password" required>
                                            <p style="color:red;font-size:13px;">
                                                @if($errors->has('password'))
                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('password')}}
                                                @endif
                                            </p>
                                        </div>
                                        <div class="form-group">
                                            <label>Yayasan<span style="color:red;"> *</span></label>
                                            <select class="form-control" name="id_yayasan" required>
                                                <option selected="true" disabled="disabled" value="{{ old('id_yayasan') }}">Pilih</option>
                                                @foreach($yayasan as $data)
                                                <option value="{{ $data->id }}">{{ $data->nama_yayasan }}</option>
                                                @endforeach
                                            </select>
                                            <p style="color:red;font-size:13px;">
                                                @if($errors->has('id_yayasan'))
                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('id_yayasan')}}
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="dt-buttons">
                                    <div class="sweetalert m-t-15">
                                        <button class="btn btn-info btn sweet-success" type="submit">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
</div>
@endsection