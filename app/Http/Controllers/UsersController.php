<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Yayasan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['user'] = User::where('role','admin')->get();
        return view('backend.user.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['yayasan'] = Yayasan::all();
        return view('backend.user.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $check = User::where('email', '=', $request->email)->exists();
        if ($check) {
            return back()->withError($request->email.' sudah di gunakan');
        }

        $input['name'] = $request->name;
        $input['email'] = $request->email;
        $input['password'] = bcrypt($request->password);
        $input['id_yayasan'] = $request->id_yayasan;
        $input['role'] = "admin";
        User::create($input);

        return redirect()->route('user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['user'] = User::find($id);
        $data['yayasan'] = User::all();
        return view('backend.user.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = User::findOrFail($id);
        $check = User::where('email', '=', $request->email)->exists();
        if ($check) {
            return back()->withError($request->email . ' sudah di gunakan');
        }
        
        if ($request->password == "") {
            $data->password = $data->password;
        }else{
            $data->password = $request->password;
        }

        if ($request->id_yayasan == "") {
            $data->id_yayasan = $data->id_yayasan;
        }else{
            $data->id_yayasan = $request->id_yayasan;
        }

        $data->name = $request->name;
        $data->email = $request->email;
        $data->update();

        return redirect()->route('user.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = User::findOrFail($id);
        $data->delete();

        return redirect()->route('user.index');
    }
}
