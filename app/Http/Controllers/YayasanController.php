<?php

namespace App\Http\Controllers;

use PDF;
use App\Exports\YayasanExport;
use App\Models\DataAnak;
use App\Models\Kesehatan;
use App\Models\Vaksinasi;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Yayasan;
use Maatwebsite\Excel\Facades\Excel;

class YayasanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['datas'] = Yayasan::all();
        return view('backend.yayasan.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.yayasan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $foto_yayasan = "";
        $foto_ktp = "";
        if ($request->hasFile('foto_yayasan') && $request->hasFile('foto_ktp_pengurus')) {
            $path = 'images';
            $file1 = $request->foto_yayasan;
            $file2 = $request->foto_ktp_pengurus;
            $extension1 = $file1->getClientOriginalExtension();
            $extension2 = $file2->getClientOriginalExtension();
            $fileName1 = rand(1111, 9999) . "." . $extension1;
            $fileName2 = rand(1111, 9999) . "." . $extension2;
            $file1->move($path, $fileName1);
            $file2->move($path, $fileName2);

            $foto_yayasan = $fileName1;
            $foto_ktp = $fileName2;
        }

        $input['nama_yayasan'] = $request->nama_yayasan;
        $input['nama_pengurus'] = $request->nama_pengurus;
        $input['no_telp_pengurus'] = $request->no_telp_pengurus;
        $input['alamat_pengurus'] = $request->alamat_pengurus;
        $input['jenis_kelamin_pengurus'] = $request->jenis_kelamin_pengurus;
        $input['foto_ktp_pengurus'] = $foto_ktp;
        $input['foto_yayasan'] = $foto_yayasan;
        $input['alamat'] = $request->alamat;
        $input['no_telp'] = $request->no_telp;
        $input['tgl_berdiri'] = $request->tgl_berdiri;
        $input['visi'] = $request->visi;
        $input['misi'] = $request->misi;
        $input['tentang_yayasan'] = $request->tentang_yayasan;
        Yayasan::create($input);

        return redirect()->route('yayasan.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['yayasan'] = Yayasan::findOrFail($id);
        return view('backend.yayasan.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['yayasan'] = Yayasan::find($id);
        return view('backend.yayasan.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Yayasan::findOrFail($id);
        // dd($request);
        if ($request->file('foto_yayasan') == "") {
            $data->foto_yayasan = $data->foto_yayasan;
        } else {
            $path = 'images';
            $file = $request->foto_yayasan;
            $extension = $file->getClientOriginalExtension();
            $fileName = rand(1111, 9999) . "." . $extension;
            $file->move($path, $fileName);

            $data->foto_yayasan = $fileName;
        }

        if ($request->file('foto_ktp_pengurus') == "") {
            $data->foto_ktp_pengurus = $data->foto_ktp_pengurus;
        } else {
            $path = 'images';
            $file = $request->foto_ktp_pengurus;
            $extension = $file->getClientOriginalExtension();
            $fileName = rand(1111, 9999) . "." . $extension;
            $file->move($path, $fileName);
            $data->foto_ktp_pengurus = $fileName;
        }
        

        if ($request->jenis_kelamin_pengurus == "") {
            $data->jenis_kelamin_pengurus = $data->jenis_kelamin_pengurus;
        }else{
            $data->jenis_kelamin_pengurus = $request->jenis_kelamin_pengurus;
        }

        $data->nama_yayasan = $request->nama_yayasan;
        $data->alamat = $request->alamat;
        $data->no_telp = $request->no_telp;
        $data->tgl_berdiri = $request->tgl_berdiri;
        $data->visi = $request->visi;
        $data->misi = $request->misi;
        $data->tentang_yayasan = $request->tentang_yayasan;
        $data->nama_pengurus = $request->nama_pengurus;
        $data->alamat_pengurus = $request->alamat_pengurus;
        $data->no_telp_pengurus = $request->no_telp_pengurus;
        $data->update();

        if (Auth::user()->role == "superadmin") {
            return redirect()->route('yayasan.index');
        } else {
            return back();
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Yayasan::findOrfail($id);
        $data->delete();

        return redirect()->route('yayasan.index');
    }

    public function profile($id)
    {
        $data['yayasan'] = Yayasan::find($id);
        return view('backend.yayasan.profile', $data);
    }

    public function updateProfile(Request $request, $id)
    {
        dd($request);
        $data = Yayasan::findOrFail($id);

        if ($request->file('foto_yayasan') == "") {
            $data->foto_yayasan = $data->foto_yayasan;
        } else {
            $path = 'images';
            $file = $request->foto_yayasan;
            $extension = $file->getClientOriginalExtension();
            $fileName = rand(1111, 9999) . "." . $extension;
            $file->move($path, $fileName);

            $data->foto_yayasan = $fileName;
        }

        if ($request->file('foto_ktp_pengurus') == "") {
            $data->foto_ktp_pengurus = $data->foto_ktp_pengurus;
        } else {
            $path = 'images';
            $file = $request->foto_ktp_pengurus;
            $extension = $file->getClientOriginalExtension();
            $fileName = rand(1111, 9999) . "." . $extension;
            $file->move($path, $fileName);
            $data->foto_ktp_pengurus = $fileName;
        }

        if ($request->jenis_kelamin_pengurus == "") {
            $data->jenis_kelamin_pengurus = $data->jenis_kelamin_pengurus;
        } else {
            $data->jenis_kelamin_pengurus = $request->jenis_kelamin_pengurus;
        }

        $data->nama_yayasan = $request->nama_yayasan;
        $data->alamat = $request->alamat;
        $data->no_telp = $request->no_telp;
        $data->tgl_berdiri = $request->tgl_berdiri;
        $data->visi = $request->visi;
        $data->misi = $request->misi;
        $data->tentang_yayasan = $request->tentang_yayasan;
        $data->nama_pengurus = $request->nama_pengurus;
        $data->alamat_pengurus = $request->alamat_pengurus;
        $data->no_telp_pengurus = $request->no_telp_pengurus;
        $data->update();

        return redirect()->route('yayasan.index');
    }

    // For Expor Excel where ID selected
    public function export($id)
    {
        return Excel::download(new YayasanExport($id), 'yayasan.csv');
    }

    // For Export PDF where ID selected
    public function pdfDataYayasan($id)
    {
        $yayasan = Yayasan::where('id',$id)->get();

        $pdf = PDF::loadView('pdf.data-yayasan',['yayasan'=>$yayasan])->setPaper('A4','landscape');
        return $pdf->stream('data-yayasan.pdf');
        // return view('pdf.data-yayasan', ['yayasan' => $yayasan]);
    }

    public function pdfDataAnak($id)
    {
        $anak = DataAnak::where('id_yayasan', $id)->get();
        $get_yayasan = DataAnak::where('id_yayasan', $id)->get()->first();

        $pdf = PDF::loadView('pdf.data-anak-per-yayasan', ['anak' => $anak, 'judul' => $get_yayasan])->setPaper('A4', 'landscape');
        return $pdf->stream('data-anak.pdf');
        // return view('pdf.data-anak-per-yayasan', ['anak' => $anak,'judul' => $get_judul_yayasan]);
    }

    public function pdfVaksinasi($id)
    {
        $vaksinasi = Vaksinasi::where('id_yayasan', $id)->get();
        $get_yayasan = Vaksinasi::where('id_yayasan', $id)->get()->first();

        $pdf = PDF::loadView('pdf.vaksinasi-per-yayasan', ['vaksinasi' => $vaksinasi,'judul'=>$get_yayasan])->setPaper('A4', 'landscape');
        return $pdf->stream('vaksinasi.pdf');
    }
 
    public function pdfKesehatan($id)
    {
        $kesehatan = Kesehatan::where('id_yayasan', $id)->get();
        $get_yayasan = Kesehatan::where('id_yayasan', $id)->get()->first();

        $pdf = PDF::loadView('pdf.kesehatan-per-yayasan', ['kesehatan' => $kesehatan,'judul'=>$get_yayasan ])->setPaper('A4', 'landscape');
        return $pdf->stream('kesehatan.pdf');
    }
}
