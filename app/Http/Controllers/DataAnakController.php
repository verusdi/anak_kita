<?php

namespace App\Http\Controllers;

use PDF;
use Auth;
use App\Exports\DataAnakExport;
use App\Models\DataAnak;
use App\Models\Kesehatan;
use App\Models\Vaksinasi;
use App\Models\Yayasan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class DataAnakController extends Controller
{


    public function cari(Request $request)
    {
        $cari = $request->cari;
        $anak = DB::table('data_anaks')->where('nama_anak','like',"%".$cari."%")->paginate();

        return view('backend.data-anak.index', ['anak' => $anak]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role == "superadmin") {
            $data['datas'] = DataAnak::all();
        } else {
            $data['datas'] = DataAnak::where('id_yayasan', Auth::user()->id_yayasan)->get();
        }
        
        return view('backend.data-anak.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['yayasan'] = Yayasan::all();
        $data['datas'] = DataAnak::all();
        return view('backend.data-anak.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $checkNik = DataAnak::where('nik', '=', $request->nik)->exists();
        $checkKode = DataAnak::where('kode_anak', '=', $request->kode_anak)->exists();
        if ($checkNik || $checkKode) {
            return back()->withError('NIK atau Kode Anak sudah tersedia');
        }

        // dd($request);
        $foto_anak = "";
        $foto_akte_lahir = "";
        $foto_kartu_keluarga = "";
        $foto_bpjs = "";
        $foto_ktp = "";
        $foto_rontgen = "";

        // Check if input is file
        if ($request->hasFile('foto_anak') && $request->hasFile('foto_akte_lahir')) {
            // Directory image
            $path = 'images';
            // Request Image From input
            $fileAnak = $request->foto_anak;
            $fileAkte = $request->foto_akte_lahir;
            //Get Extension Image
            $extension1 = $fileAkte->getClientOriginalExtension();
            $extension2 = $fileAnak->getClientOriginalExtension();
            // Change name image to Random Number
            $fileName1 = rand(1111, 9999) . "." . $extension1;
            $fileName2 = rand(1111, 9999) . "." . $extension2;
            // Move Image
            $fileAkte->move($path, $fileName1);
            $fileAnak->move($path, $fileName2);
            // Get Name Image
            $foto_akte_lahir = $fileName1;
            $foto_anak = $fileName2;
        }

        if ($request->hasFile('foto_ktp')) {
            $path = 'images';
            // Request Image From input
            $fileKtp = $request->foto_ktp;
            //Get Extension Image
            $extension = $fileKtp->getClientOriginalExtension();
            // Change name image to Random Number
            $fileName = rand(1111, 9999) . "." . $extension;
            // Move Image
            $fileKtp->move($path, $fileName);
            // Get Name Image
            $foto_ktp = $fileName;
        }

        if ($request->hasFile('foto_kartu_keluarga')) {
            $path = 'images';
            // Request Image From input
            $fileKk = $request->foto_kartu_keluarga;
            //Get Extension Image
            $extension = $fileKk->getClientOriginalExtension();
            // Change name image to Random Number
            $fileName = rand(1111, 9999) . "." . $extension;
            // Move Image
            $fileKk->move($path, $fileName);
            // Get Name Image
            $foto_kartu_keluarga = $fileName;
        }

        if ($request->hasFile('foto_kartu_bpjs')) {
            $path = 'images';
            // Request Image From input
            $fileBpjs = $request->foto_kartu_bpjs;
            //Get Extension Image
            $extension = $fileBpjs->getClientOriginalExtension();
            // Change name image to Random Number
            $fileName = rand(1111, 9999) . "." . $extension;
            // Move Image
            $fileBpjs->move($path, $fileName);
            // Get Name Image
            $foto_bpjs = $fileName;
        }

        if ($request->hasFile('foto_rontgen')) {
            $path = 'images';
            // Request Image From input
            $fileRg = $request->foto_rontgen;
            //Get Extension Image
            $extension = $fileRg->getClientOriginalExtension();
            // Change name image to Random Number
            $fileName = rand(1111, 9999) . "." . $extension;
            // Move Image
            $fileRg->move($path, $fileName);
            // Get Name Image
            $foto_rontgen = $fileName;
        }

        $input['id_yayasan'] = $request->id_yayasan;
        $input['kode_anak'] = $request->kode_anak;
        $input['nama_anak'] = $request->nama_anak;
        $input['jenis_kelamin'] = $request->jenis_kelamin;
        $input['tempat_lahir'] = $request->tempat_lahir;
        $input['kota_lahir'] = $request->kota_lahir;
        $input['tanggal_lahir'] = $request->tanggal_lahir;
        $input['alamat'] = $request->alamat;
        $input['nama_ortu'] = $request->nama_ortu;
        $input['nik_ortu'] = $request->nik_ortu;
        $input['alamat_ortu'] = $request->alamat_ortu;
        $input['no_hp_ortu'] = $request->no_hp_ortu;
        $input['berat_badan'] = $request->berat_badan;
        $input['tgl_berat_badan'] = $request->tgl_berat_badan;
        $input['tinggi_badan'] = $request->tinggi_badan;
        $input['tgl_tinggi_badan'] = $request->tgl_tinggi_badan;
        $input['massa_tubuh'] = $request->massa_tubuh;
        $input['tgl_massa_tubuh'] = $request->tgl_massa_tubuh;
        $input['suhu_badan'] = $request->suhu_badan;
        $input['tgl_suhu_badan'] = $request->tgl_suhu_badan;
        $input['tekanan_darah'] = $request->tekanan_darah;
        $input['tgl_tekanan_darah'] = $request->tgl_tekanan_darah;
        $input['nik'] = $request->nik;
        $input['gol_darah'] = $request->gol_darah;
        $input['rhesus'] = $request->rhesus;
        $input['disabilitas'] = $request->disabilitas;
        $input['alergi'] = $request->alergi;
        $input['foto_anak'] = $foto_anak;
        $input['foto_akte_lahir'] = $foto_akte_lahir;
        $input['foto_kartu_keluarga'] = $foto_kartu_keluarga;
        $input['foto_kartu_bpjs'] = $foto_bpjs;
        $input['foto_ktp'] = $foto_ktp;
        $input['foto_rontgen'] = $foto_rontgen;
        $input['catatan'] = $request->catatan;
        
        DataAnak::create($input); 
        

        // $kesehatan['id_anak'] = $request->id;
        // $kesehatan['id_yayasan'] = $request->id_yayasan;
        // $kesehatan['nama_penyakit'] = $nama_penyakit;
        // $kesehatan['gejala_penyakit'] = $request->gejala_penyakit;
        // $kesehatan['catatan_dokter'] = $request->catatan_dokter;
        // $kesehatan['terima_obat'] = $request->terima_obat;
        // $kesehatan['obat_resep'] = $request->obat_resep;
 
        // Kesehatan::create($kesehatan);

        return redirect()->route('data-anak.index')->with('success', 'Berhasil Tambah Data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['anak'] = DataAnak::findOrFail($id);
        $data['kesehatan'] = Kesehatan::where('id_anak',$id)->get();
        $data['vaksinasi'] = Vaksinasi::where('id_anak',$id)->get();
        return view('backend.data-anak.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['datas'] = DataAnak::findOrFail($id);
        $data['yayasan'] = Yayasan::all();
        return view('backend.data-anak.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = DataAnak::findOrFail($id);
        $checkNik = DataAnak::where('nik', '=', $request->nik)->exists();
        $checkKode = DataAnak::where('kode_anak', '=', $request->kode_anak)->exists();
        if ($checkNik || $checkKode) {
            return back()->withError('NIK atau Kode Anak sudah tersedia');
        }

        if ($request->hasFile('foto_akte_lahir') == "") {
            $data->foto_akte_lahir = $data->foto_akte_lahir;
        } else {
            $path = 'images';
            $fileAkte = $request->foto_akte_lahir;
            $extension = $fileAkte->getClientOriginalExtension();
            $fileName = rand(1111, 9999) . "." . $extension;
            $fileAkte->move($path, $fileName);
            $data->foto_akte_lahir = $fileName;
        }
        
        
        // Check If image null , 
        if ($request->hasFile('foto_anak') == "") {
            $data->foto_anak = $data->foto_anak;
        } else {
            // Directory image
            $path = 'images';
            // Request Image From input
            $fileAnak = $request->foto_anak;
            //Get Extension Image
            $extension = $fileAnak->getClientOriginalExtension();
            // Change name image to Random Number
            $fileName = rand(1111, 9999) . "." . $extension;
            // Move Image
            $fileAnak->move($path, $fileName);
            // Get Name Image
            $data->foto_anak = $fileName;
        }

        if ($request->hasFile('foto_ktp') == "") {
            $data->foto_ktp = $data->foto_ktp;
        } else {
            $path = 'images';
            // Request Image From input
            $fileKtp = $request->foto_ktp;
            //Get Extension Image
            $extension = $fileKtp->getClientOriginalExtension();
            // Change name image to Random Number
            $fileName = rand(1111, 9999) . "." . $extension;
            // Move Image
            $fileKtp->move($path, $fileName);
            // Get Name Image
            $data->foto_ktp = $fileName;
        }

        if ($request->hasFile('foto_kartu_keluarga') == "") {
            $data->foto_kartu_keluarga = $data->foto_kartu_keluarga;
        } else {
            $path = 'images';
            // Request Image From input
            $fileKk = $request->foto_kartu_keluarga;
            //Get Extension Image
            $extension = $fileKk->getClientOriginalExtension();
            // Change name image to Random Number
            $fileName = rand(1111, 9999) . "." . $extension;
            // Move Image
            $fileKk->move($path, $fileName);
            // Get Name Image
            $data->foto_kartu_keluarga = $fileName;
        }

        if ($request->hasFile('foto_kartu_bpjs') == "") {
            $data->foto_kartu_bpjs = $data->foto_kartu_bpjs;
        } else {
            $path = 'images';
            // Request Image From input
            $fileBpjs = $request->foto_kartu_bpjs;
            //Get Extension Image
            $extension = $fileBpjs->getClientOriginalExtension();
            // Change name image to Random Number
            $fileName = rand(1111, 9999) . "." . $extension;
            // Move Image
            $fileBpjs->move($path, $fileName);
            // Get Name Image
            $data->foto_kartu_bpjs = $fileName;
        }

        if ($request->hasFile('foto_rontgen') == "") {
            $data->foto_rontgen = $data->foto_rontgen;
        } else {
            $path = 'images';
            // Request Image From input
            $fileRg = $request->foto_rontgen;
            //Get Extension Image
            $extension = $fileRg->getClientOriginalExtension();
            // Change name image to Random Number
            $fileName = rand(1111, 9999) . "." . $extension;
            // Move Image
            $fileRg->move($path, $fileName);
            // Get Name Image
            $data->foto_rontgen = $fileName;
        }

        if ($request->jenis_kelamin == "") {
            $data->jenis_kelamin = $data->jenis_kelamin;
        } else {
            $data->jenis_kelamin = $request->jenis_kelamin;
        }

        if ($request->disabilitas == "") {
            $data->disabilitas = $data->disabilitas;
        } else {
            $data->disabilitas = $request->disabilitas;
        }

        if ($request->tempat_lahir == "") {
            $data->tempat_lahir = $data->tempat_lahir;
        } else {
            $data->tempat_lahir = $request->tempat_lahir;
        }

        if ($request->gol_darah == "") {
            $data->gol_darah = $data->gol_darah;
        } else {
            $data->gol_darah = $request->gol_darah;
        }

        $data->kode_anak = $request->kode_anak;
        $data->nama_anak = $request->nama_anak;
        $data->kota_lahir = $request->kota_lahir;
        $data->tanggal_lahir = $request->tanggal_lahir;
        $data->alamat = $request->alamat;
        $data->nama_ortu = $request->nama_ortu;
        $data->nik_ortu = $request->nik_ortu;
        $data->alamat_ortu = $request->alamat_ortu;
        $data->no_hp_ortu = $request->no_hp_ortu;
        $data->berat_badan = $request->berat_badan;
        $data->tinggi_badan = $request->tinggi_badan;
        $data->massa_tubuh = $request->massa_tubuh;
        $data->suhu_badan = $request->suhu_badan;
        $data->tekanan_darah = $request->tekanan_darah;
        $data->catatan = $request->catatan;
        $data->nik = $request->nik;
        $data->alergi = $request->alergi;
        $data->rhesus = $request->rhesus;
        $data->tgl_berat_badan = $request->tgl_berat_badan;
        $data->tgl_tinggi_badan = $request->tgl_tinggi_badan;
        $data->tgl_massa_tubuh = $request->tgl_massa_tubuh;
        $data->tgl_suhu_badan = $request->tgl_suhu_badan;
        $data->tgl_tekanan_darah = $request->tgl_tekanan_darah;
        $data->update();

        return redirect()->route('data-anak.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = DataAnak::findOrFail($id);
        $data->delete();

        return redirect()->route('data-anak.index');
    }

    // For Export Excel where ID
    public function export($id)
    {
        return Excel::download(new DataAnakExport($id),'Data-Anak.csv');
    }

    // For Export PDF where ID
    public function pdfDataAnak($id)
    {
        $anak = DataAnak::where('id', $id)->get();

        $pdf = PDF::loadView('pdf.data-anak', ['anak' => $anak])->setPaper('A4', 'landscape');
        return $pdf->stream('data-anak.pdf');

        // return view('pdf.data-anak', ['anak' => $anak]); 
    }

    public function pdfVaksinasi($id)
    {
        $vaksinasi = Vaksinasi::where('id_anak', $id)->get();
        $get_anak = Vaksinasi::where('id_anak', $id)->get()->first();

        $pdf = PDF::loadView('pdf.vaksinasi-per-anak', ['vaksinasi' => $vaksinasi, 'judul' => $get_anak])->setPaper('A4', 'landscape');
        return $pdf->download('vaksinasi.pdf');
        // return view('pdf.vaksinasi-per-anak', ['vaksinasi' => $vaksinasi, 'judul' => $get_anak]);
    }
    public function pdfKesehatan($id)
    {
        $kesehatan = Kesehatan::where('id_anak', $id)->get();
        $get_anak = Kesehatan::where('id_anak', $id)->get()->first();

        $pdf = PDF::loadView('pdf.kesehatan-per-anak', ['kesehatan' => $kesehatan,'judul' => $get_anak])->setPaper('A4', 'landscape');
        return $pdf->download('kesehatan.pdf');
    }

}
