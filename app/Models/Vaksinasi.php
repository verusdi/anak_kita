<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vaksinasi extends Model
{
   protected $fillable = [
        'id_yayasan', 
        'id_anak',
        'pemberi_vaksinasi',
        'tempat_vaksinasi',
        'alamat_tempat_vaksinasi',
        'jenis_vaksinasi',
        'vaksinasi_wajib',
        'tgl_vaksinasi_wajib',
        'vaksinasi_tambahan',
        'tgl_vaksinasi_tambahan',
        'catatan',
   ]; 

   public function dataAnak()
   {
      return $this->belongsTo('App\Models\DataAnak','id_anak','id');
   }

   public function dataYayasan()
   {
      return $this->belongsTo('App\Models\Yayasan','id_yayasan','id');
   }
}
