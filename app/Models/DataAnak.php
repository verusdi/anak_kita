<?php

namespace App\Models;

use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\Model;

class DataAnak extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    protected $guarded = ['id','_token'];
    
    protected $fillable = [
        'id_yayasan',
        'kode_anak',
        'nama_anak',
        'jenis_kelamin',
        'tempat_lahir',
        'kota_lahir',
        'tanggal_lahir',
        'disabilitas',
        'alergi',
        'alamat',
        'nama_ortu',
        'nik_ortu',
        'alamat_ortu',
        'no_hp_ortu',
        'berat_badan',
        'tinggi_badan',
        'massa_tubuh',
        'suhu_badan',
        'tekanan_darah',
        'gol_darah',
        'rhesus',
        'nik',
        'foto_akte_lahir',
        'foto_anak',
        'foto_kartu_keluarga',
        'foto_kartu_bpjs',
        'foto_ktp',
        'foto_rontgen',
        'tgl_berat_badan',
        'tgl_tinggi_badan',
        'tgl_massa_tubuh',
        'tgl_suhu_badan',
        'tgl_tekanan_darah',
        'catatan',
    ];

    public function vaksinasi()
    {
        return $this->hasMany('App\Models\Vaksinasi','id_anak','id');
    }

    public function kesehatan()
    {
        return $this->hasMany('App\Models\Kesehatan', 'id_anak', 'id');
    }
    public function yayasan()
    {
        return $this->belongsTo('App\Models\Yayasan','id_yayasan','id');
    }

    // function for onDelete Cascade
    public static function boot()
    {
        parent::boot();

        static::deleted(function ($id) {
            $id->vaksinasi()->delete();
            $id->kesehatan()->delete();
        });
    }
}
