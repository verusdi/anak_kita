<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Yayasan extends Model 
{
    protected $fillable = [
        'nama_yayasan',
        'foto_yayasan',
        'alamat',
        'no_telp',
        'tgl_berdiri',
        'visi',
        'misi',
        'tentang_yayasan',
        'nama_pengurus',
        'no_telp_pengurus',
        'alamat_pengurus',
        'jenis_kelamin_pengurus',
        'foto_ktp_pengurus',
    ];

    public function vaksinasi()
    {
        return $this->hasMany('App\Models\Vaksinasi', 'id_yayasan', 'id');
    }

    public function kesehatan()
    {
        return $this->hasMany('App\Models\Kesehatan', 'id_yayasan', 'id');
    }
    public function users()
    {
        return $this->hasMany('App\User','id_yayasan','id');
    }
    public function dataAnak()
    {
        return $this->hasMany('App\Models\DataAnak','id_yayasan','id');
    }

    // function for onDelete Cascade
    public static function boot()
    {
        parent::boot();

        static::deleted(function($yayasan){
            $yayasan->vaksinasi()->delete();
            $yayasan->kesehatan()->delete();
            $yayasan->users()->delete();
            $yayasan->dataAnak()->delete();
        });
    }
}
