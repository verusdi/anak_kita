<?php

namespace App\Exports;

use App\Models\DataAnak;
use App\Models\Kesehatan;
use App\Models\Vaksinasi;
use App\Models\Yayasan;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class YayasanExport implements FromView, WithEvents
{
    use Exportable;

    public function __construct(int $id)
    {
        $this->id = $id;        
    }
    
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        return view('export.data-yayasan',[
            'yayasan' => Yayasan::where('id',$this->id)->get(),
            'anak' => DataAnak::where('id_yayasan',$this->id)->get(),
            'kesehatan' => Kesehatan::where('id_yayasan',$this->id)->get(),
            'vaksinasi' => Vaksinasi::where('id_yayasan',$this->id)->get(),
        ]);
    }

    public function registerEvents(): array
    {
        $styleArray = [
            'font' => [
                'bold' => true,
            ]
        ];

        return [
            AfterSheet::class=>function(AfterSheet $event) use ($styleArray)
            {
                $event->sheet->getStyle('A1:K1')->applyFromArray($styleArray);
            }
        ];
    }
}
