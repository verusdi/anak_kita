<?php

use App\Models\Yayasan;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class DataAnakTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        for ($x = 1; $x <= 20; $x++) {
            DB::table('data_anaks')->insert([
                'id_yayasan' => Yayasan::all()->random()->id,
                'kode_anak' => hexdec(uniqid()),
                'nama_anak' => $faker->firstName,
                'jenis_kelamin' => $faker->randomElement(['Laki-Laki','Perempuan']),
                'tempat_lahir' => $faker->city,
                'tanggal_lahir' => $faker->date('Y-m-d'),
                'alamat' => $faker->address,
                'nama_ayah' => $faker->firstNameMale,
                'nik_ayah' => hexdec(uniqid()),
                'alamat_ayah' => $faker->address,
                'no_hp_ayah' => $faker->randomNumber(),
                'nama_ibu' => $faker->firstNameFemale,
                'nik_ibu' => hexdec(uniqid()),
                'alamat_ibu' => $faker->address,
                'no_hp_ibu' => $faker->randomNumber(),
                'nama_wali' => null,
                'nik_wali' => null,
                'alamat_wali' => null,
                'no_hp_wali' => null,
                'berat_badan' => $faker->numberBetween(10,40),
                'tgl_berat_badan' => $faker->date('Y-m-d'),
                'tinggi_badan' => $faker->numberBetween(100,180),
                'tgl_tinggi_badan' => $faker->date('Y-m-d'),
                'massa_tubuh' => $faker->numberBetween(10,80),
                'tgl_massa_tubuh' => $faker->date('Y-m-d'),
                'suhu_badan' => $faker->numberBetween(30,50),
                'tgl_suhu_badan' => $faker->date('Y-m-d'),
                'tekanan_darah' => $faker->numberBetween(100,200),
                'tgl_tekanan_darah' => $faker->date('Y-m-d'),
                'gol_darah' => $faker->randomElement(['O', 'A', 'B', 'AB']),
                'umur' => $faker->numberBetween(1,20),
            ]);
        }
    }
}
