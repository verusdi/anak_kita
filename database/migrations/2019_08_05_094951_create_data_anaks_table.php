<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataAnaksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_anaks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_yayasan')->nullable()->default(null)->unsigned();
            $table->string('nik')->unique()->nullable()->default(null);
            $table->string('kode_anak')->unique()->nullable()->default(null);
            $table->string('nama_anak')->nullable()->default(null);
            $table->string('jenis_kelamin')->nullable()->default(null);
            $table->string('tempat_lahir')->nullable()->default(null);
            $table->string('kota_lahir')->nullable()->default(null);
            $table->string('tanggal_lahir')->nullable()->default(null);
            $table->string('disabilitas')->nullable()->default(null);
            $table->string('alergi')->nullable()->default(null);
            $table->text('alamat')->nullable()->default(null);
            $table->string('nama_ortu')->nullable()->default(null);
            $table->string('nik_ortu')->nullable()->default(null);
            $table->text('alamat_ortu')->nullable()->default(null);
            $table->string('no_hp_ortu')->nullable()->default(null);
            $table->string('berat_badan')->nullable()->default(null);
            $table->string('tgl_berat_badan')->nullable()->default(null);
            $table->string('tinggi_badan')->nullable()->default(null);
            $table->string('tgl_tinggi_badan')->nullable()->default(null);
            $table->string('massa_tubuh')->nullable()->default(null);
            $table->string('tgl_massa_tubuh')->nullable()->default(null);
            $table->string('suhu_badan')->nullable()->default(null);
            $table->string('tgl_suhu_badan')->nullable()->default(null);
            $table->string('tekanan_darah')->nullable()->default(null);
            $table->string('tgl_tekanan_darah')->nullable()->default(null);
            $table->string('foto_akte_lahir')->nullable()->default(null);
            $table->string('foto_kartu_keluarga')->nullable()->default(null);
            $table->string('foto_kartu_bpjs')->nullable()->default(null);
            $table->string('foto_ktp')->nullable()->default(null);
            $table->string('foto_rontgen')->nullable()->default(null);
            $table->text('catatan')->nullable()->default(null);
            $table->string('gol_darah')->nullable()->default(null);
            $table->string('rhesus')->nullable()->default(null);
            $table->string('foto_anak')->nullable()->default(null);
            $table->timestamps();

        });



        // DB::table('data_anaks')->insert([
        //     'id_yayasan' => '1',
        //     'kode_anak' => uniqid('ID'),
        //     'nama_anak' => 'Jambrud',
        //     'jenis_kelamin' => 'Laki-Laki',
        //     'tempat_lahir' => 'Jakarta',
        //     'tanggal_lahir' => date("Y-m-d"),
        //     'alamat' => 'Jln Waru',
        //     'nama_ayah' => 'Yanto',
        //     'nik_ayah' => '018481941571',
        //     'alamat_ayah' => 'Jln Waru',
        //     'no_hp_ayah' => '089741471847',
        //     'nama_ibu' => 'Tinem',
        //     'nik_ibu' => '01718519535318',
        //     'alamat_ibu' => 'Jln Waru',
        //     'no_hp_ibu' => '0817471498',
        //     'nama_wali' => null,
        //     'nik_wali' => null,
        //     'alamat_wali' => null,
        //     'no_hp_wali' => null,
        //     'berat_badan' => '10',
        //     'tinggi_badan' => '100',
        //     'massa_tubuh' => '20',
        //     'suhu_badan' => '32',
        //     'tekanan_darah' => '160',
        // ]);

        // DB::table('data_anaks')->insert([
        //     'id_yayasan' => '2',
        //     'kode_anak' => uniqid('ID'),
        //     'nama_anak' => 'Maemunah',
        //     'jenis_kelamin' => 'Perempuan',
        //     'tempat_lahir' => 'Tangerang',
        //     'tanggal_lahir' => date("Y-m-d"),
        //     'alamat' => 'Jln Jendral sudirman',
        //     'nama_ayah' => 'Yanto',
        //     'nik_ayah' => '018481941571',
        //     'alamat_ayah' => 'Jln Jln Jendral sudirman',
        //     'no_hp_ayah' => '089741471847',
        //     'nama_ibu' => 'Tinem',
        //     'nik_ibu' => '01718519535318',
        //     'alamat_ibu' => 'Jln Jln Jendral sudirman',
        //     'no_hp_ibu' => '0817471498',
        //     'nama_wali' => null,
        //     'nik_wali' => null,
        //     'alamat_wali' => null,
        //     'no_hp_wali' => null,
        //     'berat_badan' => '10',
        //     'tinggi_badan' => '100',
        //     'massa_tubuh' => '20',
        //     'suhu_badan' => '32',
        //     'tekanan_darah' => '160',
        // ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_anaks');
    }
}
