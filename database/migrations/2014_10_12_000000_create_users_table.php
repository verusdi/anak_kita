<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Crypt;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_yayasan')->nullable()->default(null);
            $table->string('name');
            $table->string('role')->nullable()->default('admin');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        
        // DB::table('users')->insert([
        //     'id_yayasan' => '1',
        //     'name' => 'admin1',
        //     'role' => 'admin',
        //     'email' => 'pertama@admin.com',
        //     'password' => bcrypt('admin123'),
        // ]);
        
        // DB::table('users')->insert([
        //     'id_yayasan' => '2',
        //     'name' => 'admin2',
        //     'role' => 'admin',
        //     'email' => 'kedua@admin.com',
        //     'password' => bcrypt('admin123'),
        // ]);

        DB::table('users')->insert([
            'id_yayasan' => null,
            'name' => 'superadmin',
            'role' => 'superadmin',
            'email' => 'superadmin@admin.com',
            'password' => bcrypt('superadmin123'),
        ]);
        DB::table('users')->insert([
            'id_yayasan' => null,
            'name' => 'anakita',
            'role' => 'superadmin',
            'email' => 'anakita.hn@gmail.com',
            'password' => bcrypt('anakita.hn190'),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
