<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateKesehatansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kesehatans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_yayasan')->nullable()->default(null);
            $table->integer('id_anak')->nullable()->default(null);
            $table->text('nama_penyakit')->nullable()->default(null);
            $table->text('gejala_penyakit')->nullable()->default(null);
            $table->string('tanggal_pengobatan')->nullable()->default(null);
            $table->text('catatan_dokter')->nullable()->default(null);
            $table->text('obat_resep')->nullable()->default(null);
            $table->string('terima_obat')->nullable()->default(null);
            $table->timestamps();
        });

        // DB::table('kesehatans')->insert([
        //     'id_yayasan' => null,
        //     'id_anak' => null,
        //     'nama_penyakit' => null,
        //     'gejala_penyakit' => null,
        //     'catatan_dokter' => null,
        //     'obat_resep' => null,
        //     'terima_obat' => null,
        // ]);

        // DB::table('kesehatans')->insert([
        //     'id_yayasan' => '2',
        //     'id_anak' => '2',
        //     'nama_penyakit' => 'Panas, Muntah, Masuk Angin',
        //     'gejala_penyakit' => 'Panas, Muntah, Masuk Angin',
        //     'catatan_dokter' => 'Istirahat yang cukup',
        //     'obat_resep' => 'Bodrex, KukuBima',
        //     'terima_obat' => 'Ya',
        // ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kesehatans');
    }
}
